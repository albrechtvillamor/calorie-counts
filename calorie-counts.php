<?php
/*
Plugin Name: Del Frisco's Calorie Counts
Description: Calorie Counts - Add to Menus
Version: 1.0
Author: ThemeCo Enterprise
*/


// Declare Constants
define( 'CC_DIR', plugin_dir_path(  __FILE__  ) );
define( 'CC_URL', plugin_dir_url(  __FILE__  ) );


// Add files
require_once( 'settings.php' );
require_once( 'shortcodes.php' );


// The CSS
add_action( 'wp_enqueue_scripts', 'cc_load_app_style' );

function cc_load_app_style() {
		
	wp_enqueue_style( 'cc-front-styles', CC_URL . 'styles.css', array(), date('YmdHis') );	

}