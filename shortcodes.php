<?php

add_shortcode( 'cc_link', 'cc_link' );

function cc_link(){

    global $loc_arr;

	$cc_vars = array(
			'wrapper-class' => 'cc-wrapper',
			'button-class' => 'cc-link',
			'button-text' =>  'Nutrition Guide'
		);

	if(has_filter('cc_vars')) {
		$cc_vars = apply_filters('cc_vars', $cc_vars);
	}

    
    // set default pdf
    $location_pdf = get_field( "default_pdf_file" , 'option')['url'];
    $location_found = false;

    $locations_pdf_array =  get_field( "calorie_counts_pdf" , 'option');


    foreach($locations_pdf_array as $locations_pdf){
  
    	foreach($locations_pdf['locations'] as $location){

    		if($location->slug == $loc_arr['id']){
    			$location_found = true;
    			$location_pdf = $locations_pdf['pdf_file'];
    			break;
    		}

    	}

    	if($location_found == true){
    		break;
    	}
    
    }

	return '<div class="'. $cc_vars['wrapper-class'] .'"><a href="'. $location_pdf. '" class="x-btn btn-4 '. $cc_vars['button-class'] .'">'.  $cc_vars['button-text'] .'</a></div>';

}